import { fork, delay, put } from 'redux-saga/effects';
import { genresSuccess, genresError } from '../modules/genresInformation';
import json from '../../dummyData';

function* genresRequest() {
  try {
    const genresData = yield delay(1000, json);
    yield put(genresSuccess(genresData));
  } catch (error) {
    yield put(genresError(error));
  }
}

export default function* watchGenresRequest() {
  yield fork(genresRequest);
}
