import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { genresInformation } from './genresInformation';
import { steppersState } from './steppersState';

const RootReducer = (history) => {
  const router = connectRouter(history);
  return combineReducers({
    router,
    genresInformation,
    steppersState,
  });
};

export default RootReducer;
