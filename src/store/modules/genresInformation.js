export const GENRES_REQUEST = 'GENRES_REQUEST';
const GENRES_SUCCESS = 'GENRES_SUCCESS';
const GENRES_ERROR = 'GENRES_ERROR';

export const genresInformation = (state = {}, action) => {
  switch (action.type) {
    case GENRES_SUCCESS: {
      return { ...state, ...action.genresData };
    }
    case GENRES_ERROR: {
      return { ...state, error: action.error };
    }
    default: {
      return state;
    }
  }
};

export const genresRequest = () => ({
  type: GENRES_REQUEST,
});

export const genresSuccess = (genresData) => ({
  type: GENRES_SUCCESS,
  genresData,
});

export const genresError = (error) => ({
  type: GENRES_ERROR,
  error,
});
