const SET_STEP = 'SET_STEP';
const SET_GENRE = 'SET_GENRE';
const SET_SUBGENRE = 'SET_SUBGENRE';
const SET_ISNEWGENRE = 'SET_ISNEWGENRE';

const initialState = {
  currentStep: 0,
  genreIdx: null,
  subgenreIdx: null,
  inNewGenre: false,
};

export const steppersState = (state = initialState, action) => {
  switch (action.type) {
    case SET_STEP: {
      return {
        ...state,
        currentStep: action.step,
      };
    }
    case SET_GENRE: {
      return {
        ...state,
        genreIdx: action.genreIdx,
      };
    }
    case SET_SUBGENRE: {
      return {
        ...state,
        subgenreIdx: action.subgenreIdx,
      };
    }
    default: {
      return state;
    }
  }
};

export const setStep = (step) => ({
  type: SET_STEP,
  step,
});

export const setGenre = (genreIdx) => ({
  type: SET_GENRE,
  genreIdx,
});

export const setSubgenre = (subgenreIdx) => ({
  type: SET_SUBGENRE,
  subgenreIdx,
});

export const setIsNewGenre = (isNewGenre) => ({
  type: SET_ISNEWGENRE,
  isNewGenre,
});
