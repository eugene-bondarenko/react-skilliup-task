import React from 'react';
import { Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { setSubgenre } from '../../store/modules/steppersState';

import './Subgenres.css';

const Subgenres = (props) => {
  const { genreIdx, genresInformation, subgenreIdx } = props;
  console.log(props);

  const getButtonIdx = (idx) => {
    if (idx === subgenreIdx) {
      props.setSubgenre(null);
      return;
    }
    props.setSubgenre(idx);
  };

  const ButtonView = (el, idx) => (
    <div className="subgenre-button" key={Date.now() * Math.random()}>
      <Button
        onClick={() => getButtonIdx(idx)}
        color={idx === subgenreIdx ? 'primary' : 'default'}
        variant="contained"
      >
        {el.name}
      </Button>
    </div>
  );
  console.log(genresInformation.genres);
  return genresInformation.genres ? (
    <div className="subgenres-container">
      {genresInformation.genres[genreIdx].subgenres.map(ButtonView)}
    </div>
  ) : null;
};

const mapStateToProps = (state) => {
  const { steppersState, genresInformation } = state;
  const { genreIdx, subgenreIdx } = steppersState;
  return {
    genresInformation,
    genreIdx,
    subgenreIdx,
  };
};

export default connect(mapStateToProps, { setSubgenre })(Subgenres);
