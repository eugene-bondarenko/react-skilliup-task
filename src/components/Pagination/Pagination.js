import React from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { setStep } from '../../store/modules/steppersState';

import './Pagination.css';

const Pagination = (props) => {
  const { isNewGenre, currentStep } = props;

  const nextStep = () => {
    if ((isNewGenre && currentStep === 3) || (!isNewGenre && currentStep === 2)) {
      return;
    }
    props.setStep(currentStep + 1);
    console.log(currentStep);
  };

  const prevStep = () => {
    if (currentStep === 0) {
      return;
    }
    props.setStep(currentStep - 1);
    console.log(currentStep);
  };

  return (
    <div className="pagination">
      <div className="pagination-button">
        <Button onClick={prevStep} variant="outlined">
          Back
        </Button>
      </div>
      <div className="pagination-button">
        <Button onClick={nextStep} variant="contained" color="primary">
          Next
        </Button>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  const { steppersState } = state;
  const { isNewGenre, currentStep } = steppersState;

  return {
    isNewGenre,
    currentStep,
  };
};

export default connect(mapStateToProps, { setStep })(Pagination);
