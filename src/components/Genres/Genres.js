import React from 'react';
import { Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { setGenre, setSubgenre } from '../../store/modules/steppersState';
import './Genres.css';

const Genres = (props) => {
  const { genresInformation, genreIdx } = props;

  const getButtonIdx = (idx) => {
    if (idx === genreIdx) {
      props.setGenre(null);
      props.setSubgenre(null);
      return;
    }
    props.setGenre(idx);
    props.setSubgenre(null);
  };

  const ButtonView = (el, idx) => (
    <div className="genre-button" key={Date.now() * Math.random()}>
      <Button
        onClick={() => getButtonIdx(idx)}
        color={idx === genreIdx ? 'primary' : 'default'}
        variant="contained"
      >
        {el.name}
      </Button>
    </div>
  );

  return genresInformation.genres && !genresInformation.error ? (
    <div className="genres-container">{genresInformation.genres.map(ButtonView)}</div>
  ) : null;
};

const mapStateToProps = (state) => {
  const { steppersState, genresInformation } = state;
  const { genreIdx } = steppersState;
  return {
    genreIdx,
    genresInformation,
  };
};

export default connect(mapStateToProps, { setGenre, setSubgenre })(Genres);
