import React from 'react';
import { StepLabel, Step, Stepper } from '@material-ui/core';
import { connect } from 'react-redux';
import { genresRequest } from '../../store/modules/genresInformation';

const Steps = (props) => {
  const { currentStep, isNewGenre } = props;

  return (
    <>
      <Stepper alternativeLabel activeStep={currentStep}>
        <Step key={1}>
          <StepLabel>Genre</StepLabel>
        </Step>
        <Step key={2}>
          <StepLabel>Subgenre</StepLabel>
        </Step>
        {isNewGenre ? (
          <Step key={3}>
            <StepLabel>New Genre</StepLabel>
          </Step>
        ) : null}
        {currentStep < 2 && !isNewGenre ? (
          <Step key={4}>
            <StepLabel>...</StepLabel>
          </Step>
        ) : null}
        {isNewGenre || currentStep > 1 ? (
          <Step key={5}>
            <StepLabel>Information</StepLabel>
          </Step>
        ) : null}
      </Stepper>
    </>
  );
};

const mapStateToProps = (state) => {
  const { steppersState } = state;
  const { currentStep, isNewGenre } = steppersState;

  return {
    currentStep,
    isNewGenre,
  };
};

export default connect(mapStateToProps, { genresRequest })(Steps);
