import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import Genres from '../components/Genres/Genres';
import Steps from '../components/Steps/Steps';
import Pagination from '../components/Pagination/Pagination';
import Subgenres from '../components/Subgenres/Subgenres';

function Router(props) {
  const { currentStep, isNewGenre } = props;

  const navigation = (stepIdx, isGenreNew) => {
    switch (stepIdx) {
      case 0: {
        return <Genres />;
      }
      case 1: {
        return <Subgenres />;
      }
      case 2: {
        return isGenreNew ? <div>NEW GENRE</div> : <div>INFORMATION FORM</div>;
      }
      case 3: {
        return <div>INFORMATION FORM</div>;
      }
      default:
        return <div>ERROR</div>;
    }
  };

  return (
    <>
      <Steps />
      <Switch>
        <Route path="/">{navigation(currentStep, isNewGenre)}</Route>
      </Switch>
      <Pagination />
    </>
  );
}

const mapStateToProps = (state) => {
  const { steppersState } = state;
  const { currentStep, inNewGenre } = steppersState;

  return { currentStep, inNewGenre };
};

export default connect(mapStateToProps, null)(Router);
